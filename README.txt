The Kanban application can be executed in the following steps :

- First we need to create a task in thre DB. For that navigate to KanbanApplicationIntegrationTest and execute the test addKanbanTaskTest() after editing the task name , staff name of your choice
- Add one or more tasks allocated to the staff by editing and executing the test addKanbanTaskTest()
- Now to search for all BACKLOG tasks in the DB, execute the test getAllKanbanTasksTestForBacklogStatusTest()
- Keep a note of the taskId so that we can do an individual search also