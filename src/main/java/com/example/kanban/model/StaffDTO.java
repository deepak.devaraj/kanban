package com.example.kanban.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Deepak Devaraj
 * This is the Staff DTO
 */
public class StaffDTO {
	@JsonIgnore
	public int staffId;
	public String staffName;
	
	public int getStaffId() {
		return staffId;
	}
	public void setStaffId(int staffId) {
		this.staffId = staffId;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName(String staffName) {
		this.staffName = staffName;
	}
	
	

}
