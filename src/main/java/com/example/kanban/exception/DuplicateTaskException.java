package com.example.kanban.exception;

public class DuplicateTaskException extends Exception {
	
	public DuplicateTaskException(String status) {
        super("Duplicate Task exists : " + status);
    }
}
